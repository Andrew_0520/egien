Numbers = [123, 895, 119, 1037]
Matrix = [
    [1, 2, 3, 4],
    [3, 5, 9, 8],
    [8, 0, 3, 7],
    [6, 1, 9, 2]
]

class find:
    def __init__(self, numbers, matrix):
        self.numbers = numbers
        self.matrix = matrix

    def search(self, digit):
        for i in self.matrix:
            for j in i:
                if j==digit:
                    yield (self.matrix.index(i), i.index(j))

    def split_number(self, number):
        digits = []
        while number >= 1:
            if number >= 10:
                 digits.append(number % 10)
            else:
                digits.append(number)
            number = number // 10
        return digits

    def move(self, i, j, digits):
            if i<0 and self.matrix[i-1][j] == digits[0]:
                if len(digits) == 1:
                    return True
                if self.move(i-1, j, digits[1:]):
                    return True
            if i<len(self.matrix[0])-1 and self.matrix[i+1][j] == digits[0]:
                if len(digits) == 1:
                    return True
                if self.move(i+1, j, digits[1:]):
                    return True
            if j>0 and self.matrix[i][j-1] == digits[0]:
                if len(digits) == 1:
                    return True
                if self.move(i, j-1, digits[1:]):
                    return True
            if j<len(self.matrix)-1 and self.matrix[i][j+1] == digits[0]:
                if len(digits) == 1:
                    return True
                if self.move(i, j+1, digits[1:]):
                    return True
            return False

    def find_integers(self):
        new_numbers = []
        for number in self.numbers:
            digits = self.split_number(number)
            try:
                for (i, j) in self.search(digits[0]):
                    if len(digits) == 1:
                        new_numbers.append(number)
                        break
                    if self.move(i, j, digits[1:]):
                        new_numbers.append(number)
                        break
            except IndexError:
                continue
        return new_numbers

if __name__ == '__main__':
    finder = find(Numbers, Matrix)
    print(finder.find_integers())